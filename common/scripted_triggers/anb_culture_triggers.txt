﻿can_learn_language_from_culture = {
	culture = {
		has_cultural_parameter = charaters_are_bilingual
	
		any_in_list = {
			count = 1
			variable = cultural_languages
			ROOT = {
				NOT = { knows_language_of_culture = PREV }
			}
			always = yes
		}
	}
}

matches_cultures = {
	OR = {
		AND = {
			scope:culture = $culture_1$
			scope:other_culture = $culture_2$
		}
		AND = {
			scope:other_culture = $culture_1$
			scope:culture = $culture_2$
		}
	}
}