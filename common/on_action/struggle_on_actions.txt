﻿# Anbennar: commenting vanilla stuff

# root: is involved (or interloper) character
# Scope:struggle is the currently pulsing struggle.
yearly_struggle_playable_pulse = {}

# root: is involved (or interloper) character
# Scope:struggle is the currently pulsing struggle.
five_year_struggle_playable_pulse = {
	trigger = {
		# basic_is_valid_for_yearly_events_trigger = yes
		# OR = {
			# fp2_character_interloper_in_struggle_trigger = yes
			# fp2_character_involved_in_struggle_trigger = yes
		# }	
		# Anbennar
		always = no
	}
	# on_actions = { # Anbennar
		# fp2_iberian_struggle_random_events
	# }
}

fp2_iberian_struggle_random_events = {
	trigger = {
		# fp2_character_involved_in_struggle_trigger = yes
		# Anbennar
		always = no
	}
	random_events = {
		chance_to_happen = 95
		1 = 0

		# 80 = fp2_struggle.1000 #Stoking the Fire # Anbennar
		# 50 = fp2_struggle.1001 #War Widows # Anbennar
		# 50 = fp2_struggle.1010 #Soul of Iron # Anbennar
		# 45 = fp2_struggle.1020 #Death of a Councilman # Anbennar
		# 70 = fp2_struggle.2001 #Desperate Villagers Seek New Lord # Anbennar
		# #80 = fp2_struggle.2003 #Band of Lost Birds removed for FP3 # Anbennar
		# 25 = fp2_struggle.2004 #The weights inside this event are a bit high, but since the event is struggle-exclusive it will probably not be an issue # Anbennar
		# 70 = fp2_struggle.2006 #The Borders of Faith # Anbennar
		# 70 = fp2_struggle.2007 #Castle Worthy of Iberia # Anbennar
		# 5 = fp2_struggle.2009 #Catching Thieves of Myth # Anbennar
		# 90 = fp2_struggle.3001 #The Price of War Not particularly struggle related, but making it only appear during the struggle makes it more unique and special # Anbennar
		# 50 = fp2_struggle.3011 #Order of the Hatchet # Anbennar
		# 25 = fp2_struggle.3021 #The Wrath of Heaven Usually the lower chance is because the effects are too great, in this case it is to ensure people don't think every other iberian had discovered han powder. # Anbennar
	}
}

fp2_iberian_struggle_starting_events = {
	events = {
		# neutral_struggle.0001 # Anbennar
		# fp2_struggle.0003 # Anbennar
	}
}
