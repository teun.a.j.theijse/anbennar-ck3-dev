#k_bulwar
##d_bulwar
###c_bulwar
601 = {		#Bulwar

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding
	duchy_capital_building = bulwar_waterworks_01
    # History
}

###c_eduz_baranun
602 = {		#Eduz-Baranun

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_zanbar
###c_zanbar
604 = {		#Zanbar

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_kuzkumar
611 = {		#Kuzkumar

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_harklum
###c_harklum
607 = {		#Harklum

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_traz_suran
612 = {		#Traz Suran

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_kalib
###c_kalib
599 = {		#Kalib

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_azanerdu
600 = {		#Azanerdu

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_zansap
###c_zansap
596 = {		#Zansap

    # Misc
    culture = barsibu
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_surib
598 = {		#Surib

    # Misc
    culture = barsibu
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_nabasih
###c_betlibar
606 = {		#Betlibar

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_zagnuhar
605 = {		#Zagnuhar

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_elisumu
690 = {		#Elisumu

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_sareyand
###c_sareyand
625 = {		#Sareyand

    # Misc
    culture = sun_elvish
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_surzan
626 = {		#Surzan

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_duklum
627 = {		#Duklum

    # Misc
    culture = zanite
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}