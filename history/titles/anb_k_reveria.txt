﻿k_reveria = {
	825.1.1 = {
		change_development_level = 8
		holder = askeling_0105 #Askel "Reaverking"
	}
	861.3.2 = {
		holder = askeling_0104 #Lokian Askeling
	}
	903.3.27 = {
		holder = askeling_0103 #Vilhelm I Askeling
	}
	926.3.27 = {
		holder = askeling_0102 #Vilhelp II Askeling
	}
	977.11.2 = {
		holder = askeling_0101 #Alvor "The Blind" Askeling
	}
	996.4.23 = {
		holder = askeling_0001 #Daghr Askeling
	}
}

d_reaver_coast = {
	996.4.23 = {
		holder = askeling_0001 #Daghr Askeling
	}
}

c_reavers_landing = {
	996.4.23 = {
		holder = askeling_0001 #Daghr Askeling
		change_development_level = 11
	}
}

c_stormpoint = {
	996.4.23 = {
		holder = askeling_0001 #Daghr Askeling
	}
}

c_baynoms = {
	1000.1.1 = {
		change_development_level = 6
		#holder = 
	}
}

c_manerd = {
	1000.1.1 = {
		change_development_level = 6
		#holder = 
	}
}

#d_gnomish_pass = {

#}

c_olmaddit = {
	996.4.23 = {
		holder = askeling_0001 #Daghr Askeling
	}
}

c_royvibobb = {
	1000.1.1 = {
		change_development_level = 6
		#holder = 
	}
}

c_manyburrows = {
	1000.1.1 = {
		change_development_level = 6
	}
	996.4.23 = {
		holder = askeling_0006 #Sekill Askeling
	}
}
