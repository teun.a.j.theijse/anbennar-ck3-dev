﻿k_wex = {
	1000.1.1 = { change_development_level = 8 }
	850.3.9 = {
		holder = ryonard_0011	
	}
	892.4.7 = {
		holder = ryonard_0010	
	}
	897.10.7 = {
		holder = ryonard_0009	
	}
	930.2.1 = {
		holder = ryonard_0005	#Ottomac Ryonard inherits from his dad probably
	}
	984.4.10 = {
		holder = ryonard_0004	#Cedric, son of Ottomac
	}
	989.6.30= {	
		holder = ryonard_0003	#Lothane, brother of Cedric
	}
	990.10.30 = {	#Wexkeep is reseiged and their authority is lost
		holder = 0
	}
}

d_wexhills = {
	850.3.9 = {
		holder = ryonard_0011	
	}
	892.4.7 = {
		holder = ryonard_0010	
	}
	897.10.7 = {
		holder = ryonard_0009	
	}
	930.2.1 = {
		holder = ryonard_0005	#Ottomac Ryonard inherits from his dad probably
	}
	984.4.10 = {
		holder = ryonard_0004	#Cedric, son of Ottomac
	}
	989.6.30= {	
		holder = ryonard_0003	#Lothane, brother of Cedric
	}
	1014.2.16= {	
		holder = ryonard_0007	#Ottomac, son of Lothane
	}
	1021.2.20 = {
		holder = ryonard_0001 #Dyren Ryonard
	}
}

c_wexkeep = {
	1000.1.1 = { change_development_level = 12 }
	850.3.9 = {
		holder = ryonard_0011	
	}
	892.4.7 = {
		holder = ryonard_0010	
	}
	897.10.7 = {
		holder = ryonard_0009	
	}
	930.2.1 = {
		holder = ryonard_0005	#Ottomac Ryonard inherits from his dad probably
	}
	984.4.10 = {
		holder = ryonard_0004	#Cedric, son of Ottomac
	}
	989.6.30= {	
		holder = ryonard_0003	#Lothane, brother of Cedric
	}
	1014.2.16= {	
		holder = ryonard_0007	#Ottomac, son of Lothane
	}
	1021.2.20 = {
		holder = ryonard_0001 #Dyren Ryonard
	}
}

c_threeguard = {
	1000.1.1 = { change_development_level = 8 }
	850.3.9 = {
		holder = ryonard_0011	
	}
	892.4.7 = {
		holder = ryonard_0010	
	}
	897.10.7 = {
		holder = ryonard_0009	
	}
	930.2.1 = {
		holder = ryonard_0005	#Ottomac Ryonard inherits from his dad probably
	}
	984.4.10 = {
		holder = ryonard_0004	#Cedric, son of Ottomac
	}
	989.6.30= {	
		holder = ryonard_0003	#Lothane, brother of Cedric
	}
	1014.2.16= {	
		holder = ryonard_0007	#Ottomac, son of Lothane
	}
	1021.2.20 = {
		holder = ryonard_0001 #Dyren Ryonard
	}
}

c_ironhill = {
	470.1.1 = {
		liege = d_wexhills
	}
	1000.1.1 = {
		change_development_level = 8
	}
	1020.1.1 = {
		holder = robacard_0001 # Hobert Robacard
	}
}

c_wexhills = {
	470.1.1 = {
		liege = d_wexhills
	}
	940.1.1 = { 
		holder = carstenard_0003
	}
	969.4.12 = { 
		holder = carstenard_0002
	}

	1006.4.23 = { 	#Ricard Carstenard inherits it after his father dies
		holder = carstenard_0001
	}
}

# d_ottocam = {
	# 900.1.1 = {
		# holder = none
	# }
# }

c_ottocam={
	960.1.1 = {
		holder = ottocam_0001 #Ottrac Ottocam
	}
	# 978.3.18 = {
		# holder = 0
	# }
	994.1.1 = {
		holder = vinerick_0001
	}
}

c_vinerick = {
	1000.1.1 = { change_development_level = 9 }

	970.1.1 = {
		holder = vinerick_0004
	}
	# 990.1.1 = {
		# holder = 0
	# }
	994.1.1 = {
		holder = vinerick_0001
	}
}

c_indlebury = {
	1000.1.1 = { change_development_level = 10 }
	978.4.27 = { 	#Elrac the Butcher takes it in Butcher's Field Massacre or shortly after
		holder = butcherson_0002
	}

	1006.4.23 = { 	#Carstenards take it
		holder = carstenard_0001
		liege = d_wexhills
	}
}

d_bisan = {
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_bisan = {
	1000.1.1 = { change_development_level = 9 }
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_countsbridge = {
	957.2.16 = { 	
		holder = wincenard_0003
		liege = d_bisan
	}

	978.4.27 = { 	#Butcher's Field Massacre
		holder = wincenard_0002
	}

	1014.5.14 = { 	#Battle of Morban Flats bro
		holder = wincenard_0001
	}
}

c_corseton = {
	1008.4.9 = {
		holder = necropolis_0001 # High Priest Folcard
	}
}

b_the_necropolis = {
	1008.4.9 = {
		holder = necropolis_0001 # High Priest Folcard
	}
}

c_magdalaire = {
	1000.1.1 = { change_development_level = 9 }
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

d_greater_bardswood = {
	900.1.1 = {
		change_development_level = 7
	}
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_butchersfield = {
	978.4.27 = { 	#Elrac the Butcher takes it in Butcher's Field Massacre
		holder = butcherson_0002
	}

	1006.4.23 = { 	#Elrac's son inherits after Elrac dies in Relief of Wexkeep
		holder = butcherson_0001	#Roderick Butcherson
	}
}

c_bardswood = {
	1000.1.1 = { change_development_level = 7 }
}

c_stumpwood = {
	1000.1.1 = { change_development_level = 7 }
	978.4.27 = { 	#Elrac the Butcher takes it in Butcher's Field Massacre
		holder = butcherson_0002
	}

	1006.4.23 = { 	#Elrac's son inherits after Elrac dies in Relief of Wexkeep
		holder = butcherson_0001	#Roderick Butcherson
	}
}

d_sugamber = {
	897.10.7 = {
		holder = ryonard_0009	#King of Wex
	}
	920.3.14 = {
		holder = rhinmond_0001 #Rycroft síl Rhinmond, son of the dude before as cadet house
	}
	978.4.27 = { 	#Butcher's Field Massacre
		holder = rhinmond_0002
	}
}

c_rhinmond = {
	1000.1.1 = { change_development_level = 9 }
	897.10.7 = {
		holder = ryonard_0009	#King of Wex
	}
	920.3.14 = {
		holder = rhinmond_0001 #Rycroft síl Rhinmond, son of the dude before as cadet house
	}
	978.4.27 = { 	#Butcher's Field Massacre
		holder = rhinmond_0002
	}
}

d_escandar = {
	1000.1.1 = { change_development_level = 6 }
	1017.1.1 = {
		holder = 551 #Magda asil Magda
	}
}

c_escandar = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_gnollsgate = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_rupellion = {
	1004.1.29 = {
		holder = emilard_0001
	}
	1022.1.1 = {
		liege = d_escandar
	}
}

b_aelthil = {
	1015.3.21 = {
		holder = 552 #Celarion, Magda's elven husband
	}
}

b_amberflow = {
	1015.6.7 = {
		holder = moonbeam0001 #Barry, 63
	}
}